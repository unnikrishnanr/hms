/**
* Author : unni.r.krishnan@gmail.com (Unnikrishnan R), Ammachi labs
* Date   : 8th August 2016
* This file contains the definitions of the functions defined in hms_sounds.js in the google-blockly
*/

function playSound(dropdown_soundid)
{
    console.log("playSound " + dropdown_soundid);
}